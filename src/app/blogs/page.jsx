"use client"
import Link from "next/link";
import { DeleteOutlined } from '@ant-design/icons';
import { EyeInvisibleOutlined, EyeTwoTone } from '@ant-design/icons';
import axios from 'axios';
import { useEffect, useState } from "react";
import { baseUrl } from "@/env";
import { MdEdit } from "react-icons/md";
import {
  Button,
  Form,
  Input,
  InputNumber,
  Select,
  Spin,
  message,
  Popconfirm
} from 'antd';

import { Space, Table, Tag, Modal } from 'antd';
const { Column } = Table;

const formItemLayout = {
  labelCol: {
    xs: { span: 24 },
    sm: { span: 6 },
  },
  wrapperCol: {
    xs: { span: 24 },
    sm: { span: 14 },
  },
};

export default function Home() {
  const [form] = Form.useForm();
  const [isLoading, setisLoading] = useState(false);
  const [blogsData, setblogsData] = useState(undefined);
  const [perPage, setperPage] = useState(0);
  const [total, settotal] = useState(0);
  const [currentPage, setcurrentPage] = useState(1);
  const [selectedBlogStatusFilter, setselectedBlogStatusFilter] = useState(undefined);

  const fetchData = async (page =1,status,) => {
    console.log(currentPage)
    const token = localStorage.getItem('token');
    setisLoading(true);
  
    try {
      const response = await axios.get(`${baseUrl}/blogs`, {
        params: {
          page,
          status,
          "order[id]":'asc'
        },
        headers: {
          'Authorization': `Bearer ${token}`
        }
      });
  
      if (response.data.status === 200) {
        console.log(response.data.data);
        setperPage(response.data.meta.per_page);
        setcurrentPage(response.data.meta.current_page);
        settotal(response.data.meta.total);
        setblogsData(response.data.data);
      }
        } catch (error) {
      if (error.response.status === 401) {
        router.push('/login')
      }
      console.log(error);
    } finally {
      setisLoading(false);
      setselectedBlogStatusFilter(status)
    }
  };

  const handleOnPageChange = (page) => {
    fetchData(page, selectedBlogStatusFilter)
  }
  const onBlogsStatusChange = (value) => {
    fetchData(1, value)
  };


  useEffect(() => {
    document.title = 'Home'
    fetchData()
  }, [])

  const [open, setOpen] = useState(false);
  const [confirmLoading, setConfirmLoading] = useState(false);

  const [selectedBlog, setselectedBlog] = useState('')

  const showModal = () => {
    setisEdit(false)
    setselectedBlog(null)
    setOpen(true);
  };


  useEffect(() => {
    console.log(form)
    if (!open) {
      form.resetFields();
    }
  }, [open, form]);


  const editBlog = async (values) => {
    setConfirmLoading(true);
    console.log(values)
    try {
      const token = localStorage.getItem('token');
      const response = await axios.put(`${baseUrl}/blogs/${selectedBlog.id}`, values, {
        headers: {
          'Authorization': `Bearer ${token}`,
        },
      });
      if (response.data.status === 200) {
        fetchData()
        setOpen(false);
        setimage(null)
        setisEdit(false)
        setselectedBlog(null)
        message.success('Success')
      }
        } catch (error) {
      if (error.response.status === 401) {
        router.push('/login')
      }
      console.log(error);
    } finally {
      setConfirmLoading(false);
    }
  };

  const [image, setimage] = useState(null);


  const onImageChange = (e) => {
    setimage(e.target.files[0]);
  };

  const addBlog = async (values) => {
    setConfirmLoading(true);
    console.log(values)

    const formData = new FormData();
    formData.append('title', values.title);
    formData.append('content', values.content);
    formData.append('is_active', values.is_active);
    formData.append('image', image);
    formData.append('url', values.url);
    try {
      const token = localStorage.getItem('token');
      const response = await axios.post(`${baseUrl}/blogs`, formData, {
        headers: {
          'Authorization': `Bearer ${token}`,
          'Content-Type': 'multipart/form-data',
        },
      });
      if (response.data.status === 200) {
        fetchData()
        setOpen(false);
        setimage(null)
        setisEdit(false)
        setselectedBlog(null)
        message.success('Success')
      }
        } catch (error) {
      if (error.response.status === 401) {
        router.push('/login')
      }
      message.error(error.message)
    } finally {
      setConfirmLoading(false);
    }
  };

  const handleCancel = () => {
    setOpen(false);
    setimage(null)
    setisEdit(false)
    setselectedBlog(null)
  };

  const confirmDelete = async (e, id) => {
    try {
      const token = localStorage.getItem('token');
      const response = await axios.delete(`${baseUrl}/blogs/${id}`, {
        headers: {
          'Authorization': `Bearer ${token}`,
        },
      });
      if (response.data.status === 200) {
        message.success('Deleted Successfully');
        fetchData()
      }
        } catch (error) {
      if (error.response.status === 401) {
        router.push('/login')
      }
      console.log(error);
      message.success(error);
    } finally {
      setConfirmLoading(false);
    }
  };

  const [isEdit, setisEdit] = useState(false)

  const onBlogEdit = (blog) => {
    setisEdit(true)
    form.setFieldsValue({
      title: blog.title,
      content: blog.content,
      is_active: parseInt(blog.is_active),
      url: blog.url,
    });

    setselectedBlog(blog)

    setOpen(true);
  }

  return (
    <main className="flex min-h-[85.75vh] flex-col items-center p-0 overflow-x-hidden bg-white">

      <div className="w-full flex py-4 justify-between">
        <div className="flex gap-2 items-center">
          <Select 
            placeholder="Blog status"
            value={selectedBlogStatusFilter}
            onChange={value => onBlogsStatusChange(value)}
            options={[
              { value: 1, label: 'Active' },
              { value: 0, label: 'Inactive' },
            ]}
            rules={[{ required: true, message: 'This field is Required!' }]}
            />
          <Button type="primary" onClick={() => fetchData()} className="bg-main_blue-100">Reset Filters</Button>
          
        </div>
        <>
          <Button type="primary" onClick={showModal} className="bg-main_blue-100">Add Blog</Button>
          <Modal title={'Add Blog'} open={open} confirmLoading={confirmLoading} footer={null} onCancel={handleCancel}>

            {open && <Form {...formItemLayout} variant="filled" style={{ maxWidth: 600 }} onFinish={!isEdit ? addBlog : editBlog} form={form}>
              <Form.Item 
              hasFeedback
              label="Title"
              name="title"
              validateDebounce={1000}
              rules={[{ required: true, message: 'This field is Required!' }]}
              >
                <Input />
              </Form.Item>

              <Form.Item 
              hasFeedback
              label="Content"
              name="content"
              validateDebounce={1000}
              rules={[{ required: true, message: 'This field is Required!' }]}
              >
                <Input />
              </Form.Item>

              <Form.Item
                hasFeedback
                label="Image"
                name="image"
                rules={[!isEdit && { required: true, message: 'Please upload an Excel file!' },]}
              >
                <Input type="file" accept=".png, .jpg, .jpeg, .svg, .webp" onChange={e => onImageChange(e)}/>
              </Form.Item>

              <Form.Item label="Is Active" name="is_active" rules={[{ required: true, message: 'This field is Required!' }]}>
                <Select 
                options={[
                  { value: 1, label: 'Active' },
                  { value: 0, label: 'Inactive' },
                ]}
                />
              </Form.Item>

              <Form.Item 
              hasFeedback
              label="Url"
              name="url"
              validateDebounce={1000}
              rules={[{ type: 'url', message: 'The input is not a valid Url!' }]}
              >
                <Input />
              </Form.Item>


              <Form.Item wrapperCol={{ offset: 6, span: 16 }}>
              <div className="flex gap-4">
                <Button type="primary" htmlType="submit">
                  Submit
                </Button>
                <Button onClick={handleCancel}>
                  cancel
                </Button>
              </div>
              </Form.Item>
            </Form>}

          </Modal>
        </>
      </div>

      <div className="flex w-full">
        {!isLoading ? <Table className="w-full" dataSource={blogsData} 
        pagination={{
          current: currentPage,
          pageSize: perPage,
          total:total,
          onChange: handleOnPageChange
        }}>
          <Column title="ID" dataIndex="id" key="id" />
          <Column title="Image Url" dataIndex="image_url" key="image_url" />
          <Column title="Title" dataIndex="title" key="title" />
          <Column title="Content" dataIndex="content" key="content" />
          <Column title="Url" dataIndex="url" key="url" />
          <Column
            title="Tags"
            dataIndex="is_active"
            key="is_active"
            render={(is_active) => (
              <>
                <Tag color={parseInt(is_active) === 1 ? 'green' : 'volcano'} key={parseInt(is_active)}>
                  {parseInt(is_active) === 1 ? 'Active' : 'Inactive'}
                </Tag>
              </>
            )}
          />
          <Column
            title="Action"
            key="action"
            render={(_, record) => (
              <Space size="middle" key={record.id}>
                <MdEdit onClick={() => onBlogEdit(record)} className="transition hover:scale-125 text-main_blue-100 cursor-pointer"/>

                <Popconfirm
                  title="Delete"
                  description="Are you sure to delete?"
                  onConfirm={(e) => confirmDelete(e, record.id)}
                  okText="Yes"
                  cancelText="No"
                >
                  <DeleteOutlined className="transition hover:scale-125 text-main_color-100 cursor-pointer"/>
                </Popconfirm>
              </Space>
            )}
          />
        </Table> 
        : <div className="flex w-full justify-center items-center p-8">
          <Spin />
        </div>
        }
      </div>

    </main>
  )
}
