"use client"
import { Inter } from 'next/font/google'
import './Styles/globals.css'
import './Styles/Home.css'
import './Styles/login.css'
import './Styles/HowItWork.css'
import Navbar from './components/Navbar'
import { ThemeProvider } from "./themeClient";

import {
    MenuFoldOutlined,
    MenuUnfoldOutlined,
    LogoutOutlined,
    TeamOutlined,
    UserOutlined,
    FileTextOutlined,
} from '@ant-design/icons';
import { MdMiscellaneousServices } from "react-icons/md";
import { FaChartPie } from "react-icons/fa";
import { Button, Layout, Menu, theme } from 'antd';
import { useEffect, useState } from 'react'
import { usePathname } from 'next/navigation'
import { loginUrl } from '@/env'
import Link from 'next/link'
import axios from 'axios'
import { useRouter } from 'next/navigation'
const { Header, Sider, Content } = Layout;

    function getItem(label, key, icon, link, children) {
        return {
            label: (
                <Link legacyBehavior href={link}>
                <a className="text-white hover:text-main_color-100 mr-4">{label}</a>
            </Link>
            ),
            key,
            icon,
            children,
        };
    }
    const items = [
    getItem('Users', '1', <TeamOutlined />, '/'),
    getItem('Blogs', '2', <FileTextOutlined />, '/blogs'),
    getItem('Services', '3', <MdMiscellaneousServices />, '/services'),
    getItem('Statistics', '4', <FaChartPie />, '/statistics'),
    {
        label: 'Settings',
        key: 'sub1',
        icon: <UserOutlined />,
        children:[
            getItem('General', '5', '', '/settings/general'),
            getItem('Banner', '6', '', '/settings/banner'),
            getItem('About', '7', '', '/settings/about'),
            getItem('Destination', '8', '', '/settings/destination'),
            getItem('Choose', '9', '', '/settings/choose'),
            getItem('Started', '10', '', '/settings/started'),
        ]
    }
    ];

    
    
    const inter = Inter({ subsets: ['latin'] })
    
    export default function RootLayout({ children }) {
    const router = useRouter()

    const Pathname = usePathname()
    const [IsLogedIn, setIsLogedIn] = useState(true)
    const [pageName, setpageName] = useState('true')
    const [selectedKeys, setselectedKeys] = useState(['1'])

    const setThePageName = () => {
        switch (Pathname) {
            case '/':
                setpageName('Users');
                setselectedKeys(['1'])
                break;
            case '/login':
                setpageName('Users');
                setselectedKeys(['1'])
                logout()
                break;
            case '/blogs':
                setpageName('Blogs');
                setselectedKeys(['2'])
                break;
            case '/services':
                setpageName('Services');
                setselectedKeys(['3'])
                break;
            case '/statistics':
                setpageName('Statistics');
                setselectedKeys(['4'])
                break;
            case '/settings/general':
                setpageName('General');
                setselectedKeys(['sub1','5'])
                break;
            case '/settings/banner':
                setpageName('Banner');
                setselectedKeys(['sub1','6'])
                break;
            case '/settings/about':
                setpageName('About');
                setselectedKeys(['sub1','7'])
                break;
            case '/settings/destination':
                setpageName('Destination');
                setselectedKeys(['sub1','8'])
                break;
            case '/settings/choose':
                setpageName('Choose');
                setselectedKeys(['sub1','9'])
                break;
            case '/settings/started':
                setpageName('Started');
                setselectedKeys(['sub1','10'])
                break;
        
            default:
                setpageName('Users');
                break;
        }
    };

    useEffect(() => {
    setIsLogedIn(!!localStorage.getItem('token'))
    setThePageName()
    }, [Pathname])

    useEffect(() => {
    if (Pathname === '/login') {
        logout()
    }
    }, [])

    

    const logout = async () => {
        const token = localStorage.getItem('token');
        if(token) {
            try {
            const response = await axios.post(`${loginUrl}/auth/logout`, {
                headers: {
                'Authorization': `Bearer ${token}`,
                },
            });
            if (response.data.status === 200) {
                localStorage.removeItem('token');
            }
            } catch (error) {
                if (error.response.status === 401) {
                    router.push('/login')
                }
                console.log(error);
            } finally {
                localStorage.removeItem('token');
            }
        }
    };


    const [collapsed, setCollapsed] = useState(false);
    const {
        token: { colorBgContainer, borderRadiusLG },
    } = theme.useToken();


return (
    <html lang="en">
            <body className={inter.className}>
                <ThemeProvider>
                    <Layout className='max-w-[100vw] overflow-x-hidden'>
                    {Pathname != '/login' && <Sider  trigger={null} collapsible collapsed={collapsed}>
                        <div className="demo-logo-vertical" />
                        <div className='flex w-full p-4 items-center justify-center'>
                            {collapsed ? <p className='text-white font-semibold text-4xl'>R</p> : <p className='text-white font-semibold text-4xl'>Rabbit</p>}
                        </div>
                        <Menu
                        theme="dark"
                        mode="inline"
                        defaultSelectedKeys={['1']}
                        selectedKeys={selectedKeys}
                        defaultOpenKeys={['sub1']}
                        items={items}
                        />
                        <div className='w-full px-4 pt-8 flex justify-center items-center'>
                            <Button className='p-2 px-4' type="primary" icon={<LogoutOutlined />} onClick={() => logout()}>{collapsed ? '' : ' Logout '}</Button>
                        </div>
                    </Sider>}
                    <Layout>
                        {Pathname != '/login' && <Header style={{padding: 0,background: colorBgContainer,}}>
                            <div className='flex items-center gap-2 h-full'>
                                <Button 
                                type="text" icon={collapsed ? <MenuUnfoldOutlined /> : <MenuFoldOutlined />}  
                                onClick={() => setCollapsed(!collapsed)}  style={{fontSize: '16px',width: 64,height: 64,}}/>
                                <p className='text-xl font-semibold'>{pageName}</p>
                            </div>
                        </Header>}
                        <Content
                        style={{margin: '24px 16px',padding: 24,minHeight: 280,background: colorBgContainer,borderRadius: borderRadiusLG,}}>
                            {children}
                        </Content>
                    </Layout>
                    </Layout>
                </ThemeProvider>
            </body>
    </html>
)
}