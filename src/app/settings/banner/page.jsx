"use client"
import axios from 'axios';
import { useEffect, useState } from "react";
import { baseUrl } from "@/env";
import {
  Button,
  Form,
  Input,
  Spin,
  message,
} from 'antd';

const formItemLayout = {
  labelCol: {
    xs: { span: 24 },
    sm: { span: 6 },
  },
  wrapperCol: {
    xs: { span: 24 },
    sm: { span: 14 },
  },
};

export default function Home() {
  const [form] = Form.useForm();
  const [isLoading, setisLoading] = useState(false);

  const fetchData = async () => {
    const token = localStorage.getItem('token');
    setisLoading(true);
  
    try {
      const response = await axios.get(`${baseUrl}/settings`, {
        headers: {
          'Authorization': `Bearer ${token}`
        }
      });
  
      if (response.data.status === 200) {
        const data = response.data.data.find(d => d.slug == 'banner').value
        if(data)
        form.setFieldsValue({
          header: data.header,
          content: data.content,
          footer: data.footer,
        });
      }
        } catch (error) {
      if (error.response.status === 401) {
        router.push('/login')
      }
      console.log(error);
    } finally {
      setisLoading(false);
    }
  };


  useEffect(() => {
    document.title = 'Banner'
    fetchData()
  }, [])

  const [image, setimage] = useState(null);


  const onImageChange = (e) => {
    setimage(e.target.files[0]);
  };

  const editBanner = async (values) => {
    setisLoading(true);
    console.log(values)

    const formData = new FormData();
    if (values.header) formData.append('banner[header]', values.header);
    if (values.content) formData.append('banner[content]', values.content);
    if (values.footer) formData.append('banner[footer]', values.footer);
    if(image) formData.append('banner[image]', image);
    try {
      const token = localStorage.getItem('token');
      const response = await axios.post(`${baseUrl}/settings`, formData, {
        headers: {
          'Authorization': `Bearer ${token}`,
          'Content-Type': 'multipart/form-data',
        },
      });
      if (response.data.status === 200) {
        form.resetFields()
        setimage(undefined)
        fetchData()
        message.success('Success')
      }
        } catch (error) {
      if (error.response.status === 401) {
        router.push('/login')
      }
      message.error(error.message);
    } finally {
      setisLoading(false);
    }
  };


  return (
    <main className="flex min-h-[85.75vh] flex-col items-center p-0 overflow-x-hidden bg-white">

      <div className="flex w-full">
        {!isLoading ?             
        <Form {...formItemLayout} variant="filled" style={{ maxWidth: 600 }} className="w-96" onFinish={editBanner} form={form}>


          <Form.Item 
          hasFeedback
          label="Header"
          name="header"
          validateDebounce={1000}>
            <Input />
          </Form.Item>

          <Form.Item 
          hasFeedback
          label="Content"
          name="content"
          validateDebounce={1000}>
            <Input />
          </Form.Item>

          <Form.Item 
          hasFeedback
          label="Footer"
          name="footer"
          validateDebounce={1000}>
            <Input />
          </Form.Item>

          <Form.Item
            hasFeedback
            label="Image"
            name="image"
          >
            <Input type="file" accept=".png, .jpg, .jpeg, .svg, .webp" onChange={e => onImageChange(e)}/>
          </Form.Item>

              <Form.Item wrapperCol={{ offset: 6, span: 16 }}>
              <div className="flex gap-4">
                <Button type="primary" htmlType="submit">
                  Edit
                </Button>
              </div>
              </Form.Item>
            </Form>
        : <div className="flex w-full justify-center items-center p-8">
          <Spin />
        </div>
        }
      </div>
    </main>
  )
}
