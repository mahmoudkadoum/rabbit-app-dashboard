"use client"
import axios from 'axios';
import { useEffect, useState } from "react";
import { baseUrl } from "@/env";
import {
  Button,
  Form,
  Input,
  InputNumber,
  Spin,
  message,
} from 'antd';
import { useRouter } from "next/navigation";

const formItemLayout = {
  labelCol: {
    xs: { span: 24 },
    sm: { span: 6 },
  },
  wrapperCol: {
    xs: { span: 24 },
    sm: { span: 14 },
  },
};

export default function Home() {

  const router = useRouter();
  const [form] = Form.useForm();
  const [isLoading, setisLoading] = useState(false);

  const fetchData = async () => {
    const token = localStorage.getItem('token');
    setisLoading(true);
  
    try {
      const response = await axios.get(`${baseUrl}/settings`, {
        headers: {
          'Authorization': `Bearer ${token}`
        }
      });
  
      if (response.data.status === 200) {
        const data = response.data.data.find(d => d.slug == 'general').value
        if(data)
        form.setFieldsValue({
          email: data.email,
          phone: data.phone,
        });
      }
    } catch (error) {
      console.log(error.response.status)
      if (error.response.status === 401) {
        router.push('/login')
      }
    } finally {
      setisLoading(false);
    }
  };


  useEffect(() => {
    document.title = 'General'
    fetchData()
  }, [])

  const editGeneral = async (values) => {
    setisLoading(true);
    console.log(values)

    const formData = new FormData();
    formData.append('general[email]', values.email);
    formData.append('general[phone]', values.phone);
    try {
      const token = localStorage.getItem('token');
      const response = await axios.post(`${baseUrl}/settings`, formData, {
        headers: {
          'Authorization': `Bearer ${token}`,
        },
      });
      if (response.data.status === 200) {
        fetchData()
        message.success('Success')
      }
        } catch (error) {
      if (error.response.status === 401) {
        router.push('/login')
      }
      message.error(error.message);
    } finally {
      setisLoading(false);
    }
  };


  return (
    <main className="flex min-h-[85.75vh] flex-col items-center p-0 overflow-x-hidden bg-white">

      <div className="flex w-full">
        {!isLoading ?             
        <Form {...formItemLayout} variant="filled" style={{ maxWidth: 600 }} className="w-96" onFinish={editGeneral} form={form}>


          <Form.Item 
          hasFeedback
          label="Email"
          name="email"
          validateDebounce={1000}
          rules={[{ type: 'email', message: 'The input is not a valid email!' }]}>
            <Input />
          </Form.Item>

          <Form.Item
            label="Phone"
            name="phone"
            validateDebounce={1000}
            rules={[
              {
                validator: (_, value) => {
                  if (!value || ((value).toString().length >= 8 && (value).toString().length <= 12)) {
                    return Promise.resolve();
                  }
                  return Promise.reject(new Error('Phone number must be between 8 and 12 characters long'));
                },
              }
            ]}
          >
            <InputNumber style={{ width: '100%' }} />
          </Form.Item>

              <Form.Item wrapperCol={{ offset: 6, span: 16 }}>
              <div className="flex gap-4">
                <Button type="primary" htmlType="submit">
                  Edit
                </Button>
              </div>
              </Form.Item>
            </Form>
        : <div className="flex w-full justify-center items-center p-8">
          <Spin />
        </div>
        }
      </div>
    </main>
  )
}
