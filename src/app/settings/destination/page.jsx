"use client"
import axios from 'axios';
import { useEffect, useState } from "react";
import { baseUrl } from "@/env";
import {
  Button,
  Form,
  Input,
  Spin,
  message,
} from 'antd';

const formItemLayout = {
  labelCol: {
    xs: { span: 24 },
    sm: { span: 6 },
  },
  wrapperCol: {
    xs: { span: 24 },
    sm: { span: 14 },
  },
};

export default function Home() {
  const [form] = Form.useForm();
  const [isLoading, setisLoading] = useState(false);

  const fetchData = async () => {
    const token = localStorage.getItem('token');
    setisLoading(true);
  
    try {
      const response = await axios.get(`${baseUrl}/settings`, {
        headers: {
          'Authorization': `Bearer ${token}`
        }
      });
  
      if (response.data?.status === 200) {
        const data = response.data.data.find(d => d.slug == 'destination').value
        if (data)
        form.setFieldsValue({
          header: data.header,

          first: data.list[0],
          second: data.list[1],
          third: data.list[2],
          fourth: data.list[3],
        });
      }
        } catch (error) {
      if (error.response?.status === 401) {
        router.push('/login')
      }
      console.log(error);
    } finally {
      setisLoading(false);
    }
  };


  useEffect(() => {
    document.title = 'Destination'
    fetchData()
  }, [])

  const editDestination = async (values) => {
    setisLoading(true);

    const formData = new FormData();
    if(values.header) formData.append('destination[header]', values.header);
    
    if (values.first) formData.append('destination[list][0]', values.first);
    if (values.second) formData.append('destination[list][1]', values.second);
    if (values.third) formData.append('destination[list][2]', values.third);
    if (values.fourth) formData.append('destination[list][3]', values.fourth);
    try {
      const token = localStorage.getItem('token');
      const response = await axios.post(`${baseUrl}/settings`, formData, {
        headers: {
          'Authorization': `Bearer ${token}`,
        },
      });
      if (response.data?.status === 200) {
        fetchData()
        message.success('Success')
      }
        } catch (error) {
      if (error.response?.status === 401) {
        router.push('/login')
      }
      message.error(error.message);
    } finally {
      setisLoading(false);
    }
  };


  return (
    <main className="flex min-h-[85.75vh] flex-col items-center p-0 overflow-x-hidden bg-white">

      <div className="flex w-full">
        {!isLoading ?             
        <Form {...formItemLayout} variant="filled" style={{ maxWidth: 600 }} className="w-96" onFinish={editDestination} form={form}>

        <Form.Item 
          hasFeedback
          label="Header"
          name="header"
          validateDebounce={1000}
          rules={[{ required: true, message: 'This field is Required!' }]}>
            <Input placeholder='Header'/>
          </Form.Item>

          <p className='text-sm font-thin ps-4'>Destination Content List</p>

          <Form.Item 
          hasFeedback
          label="First"
          name="first">
            <Input placeholder='First'/>
          </Form.Item>

          <Form.Item 
          hasFeedback
          label="Second"
          name="second">
            <Input placeholder="Second"/>
          </Form.Item>

          <Form.Item 
          hasFeedback
          label="Third"
          name="third">
            <Input placeholder="Third"/>
          </Form.Item>

          <Form.Item 
          hasFeedback
          label="Fourth"
          name="fourth">
            <Input placeholder="Fourth"/>
          </Form.Item>

              <Form.Item wrapperCol={{ offset: 6, span: 16 }}>
              <div className="flex gap-4">
                <Button type="primary" htmlType="submit">
                  Edit
                </Button>
              </div>
              </Form.Item>
            </Form>
        : <div className="flex w-full justify-center items-center p-8">
          <Spin />
        </div>
        }
      </div>
    </main>
  )
}
