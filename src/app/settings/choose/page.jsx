"use client"
import axios from 'axios';
import { useEffect, useState } from "react";
import { baseUrl } from "@/env";
import {
  Button,
  Form,
  Input,
  Spin,
  message,
} from 'antd';

const formItemLayout = {
  labelCol: {
    xs: { span: 24 },
    sm: { span: 6 },
  },
  wrapperCol: {
    xs: { span: 24 },
    sm: { span: 14 },
  },
};

export default function Home() {
  const [form] = Form.useForm();
  const [isLoading, setisLoading] = useState(false);

  const fetchData = async () => {
    const token = localStorage.getItem('token');
    setisLoading(true);
  
    try {
      const response = await axios.get(`${baseUrl}/settings`, {
        headers: {
          'Authorization': `Bearer ${token}`
        }
      });
  
      if (response.data?.status === 200) {
        // console.log(response.data.data.find(d => d.slug = 'choose').value,);
        const data = response.data.data.find(d => d.slug === 'choose').value
        console.log(data)
        if (data)
        form.setFieldsValue({
          header: data?.header,

          firstHeader: data.list[0]?.header,
          firstContent: data.list[0]?.content,
          firstUrl: data.list[0]?.link,

          secondHeader: data.list[1]?.header,
          secondContent: data.list[1]?.content,
          secondUrl: data.list[1]?.link,

          thirdHeader: data.list[2]?.header,
          thirdContent: data.list[2]?.content,
          thirdUrl: data.list[2]?.link,

          fourthHeader: data.list[3]?.header,
          fourthContent: data.list[3]?.content,
          fourthUrl: data.list[3]?.link,
        });
      }
    } catch (error) {
      console.log(error)
      if (error.response?.status === 401) {
        router.push('/login')
      }
    } finally {
      setisLoading(false);
    }
  };


  useEffect(() => {
    document.title = 'Choose'
    fetchData()
  }, [])

  const editChoose = async (values) => {
    setisLoading(true);
    console.log(values)

    const formData = new FormData();
    if(values.header) formData.append('choose[header]', values.header);
    
    if (values.firstHeader) formData.append('choose[list][0][header]', values.firstHeader);
    if (values.firstContent) formData.append('choose[list][0][content]', values.firstContent);
    if (values.firstUrl) formData.append('choose[list][0][link]', values.firstUrl);
    
    if (values.secondHeader) formData.append('choose[list][1][header]', values.secondHeader);
    if (values.secondContent) formData.append('choose[list][1][content]', values.secondContent);
    if (values.secondUrl) formData.append('choose[list][1][link]', values.secondUrl);
    
    if (values.thirdHeader) formData.append('choose[list][2][header]', values.thirdHeader);
    if (values.thirdContent) formData.append('choose[list][2][content]', values.thirdContent);
    if (values.thirdUrl) formData.append('choose[list][2][link]', values.thirdUrl);
    
    if (values.fourthHeader) formData.append('choose[list][3][header]', values.fourthHeader);
    if (values.fourthContent) formData.append('choose[list][3][content]', values.fourthContent);
    if (values.fourthUrl) formData.append('choose[list][3][link]', values.fourthUrl);

    try {
      const token = localStorage.getItem('token');
      const response = await axios.post(`${baseUrl}/settings`, formData, {
        headers: {
          'Authorization': `Bearer ${token}`,
        },
      });
      if (response.data?.status === 200) {
        fetchData()
        message.success('Success')
      }
        } catch (error) {
      if (error.response?.status === 401) {
        router.push('/login')
      }
      message.error(error.message);
    } finally {
      setisLoading(false);
    }
  };


  return (
    <main className="flex min-h-[85.75vh] flex-col items-center p-0 overflow-x-hidden bg-white">

      <div className="flex w-full">
        {!isLoading ?             
        <Form {...formItemLayout} variant="filled" style={{ maxWidth: 600 }} className="w-96" onFinish={editChoose} form={form}>


          <Form.Item 
          hasFeedback
          label="Header"
          name="header"
          validateDebounce={1000}>
            <Input placeholder='Header'/>
          </Form.Item>

          <p className='text-sm font-thin ps-4'>First choose:</p>

          <Form.Item 
          hasFeedback
          label="Header"
          name="firstHeader"
          validateDebounce={1000}>
            <Input placeholder='Header'/>
          </Form.Item>

          <Form.Item 
          hasFeedback
          label="Content"
          name="firstContent"
          validateDebounce={1000}>
            <Input placeholder="Content"/>
          </Form.Item>

          <Form.Item 
          hasFeedback
          label="Url"
          name="firstUrl"
          validateDebounce={1000}
          rules={[{ type: 'url', message: 'The input is not a valid Url!' }]}
          >
            <Input placeholder="Url"/>
          </Form.Item>

          <p className='text-sm font-thin ps-4'>Second choose:</p>

          <Form.Item 
          hasFeedback
          label="Header"
          name="secondHeader"
          validateDebounce={1000}>
            <Input placeholder='Header'/>
          </Form.Item>

          <Form.Item 
          hasFeedback
          label="Content"
          name="secondContent"
          validateDebounce={1000}>
            <Input placeholder="Content"/>
          </Form.Item>

          <Form.Item 
              hasFeedback
              label="Url"
              name="usecondUl"
              validateDebounce={1000}
              rules={[{ type: 'url', message: 'The input is not a valid Url!' }]}
              >
                <Input placeholder="Url"/>
              </Form.Item>

          <p className='text-sm font-thin ps-4'>Third choose:</p>

          <Form.Item 
          hasFeedback
          label="Header"
          name="thirdHeader"
          validateDebounce={1000}>
            <Input placeholder='Header'/>
          </Form.Item>

          <Form.Item 
          hasFeedback
          label="Content"
          name="thirdContent"
          validateDebounce={1000}>
            <Input placeholder="Content"/>
          </Form.Item>

          <Form.Item 
              hasFeedback
              label="Url"
              name="thirdUrl"
              validateDebounce={1000}
              rules={[{ type: 'url', message: 'The input is not a valid Url!' }]}
              >
                <Input placeholder="Url"/>
              </Form.Item>

          <p className='text-sm font-thin ps-4'>Fourth choose:</p>

          <Form.Item 
          hasFeedback
          label="Header"
          name="fourthHeader"
          validateDebounce={1000}>
            <Input placeholder='Header'/>
          </Form.Item>

          <Form.Item 
          hasFeedback
          label="Content"
          name="fourthContent"
          validateDebounce={1000}>
            <Input placeholder="Content"/>
          </Form.Item>

          <Form.Item 
              hasFeedback
              label="Url"
              name="fourthUrl"
              validateDebounce={1000}
              rules={[{ type: 'url', message: 'The input is not a valid Url!' }]}
              >
                <Input placeholder="Url"/>
              </Form.Item>

              <Form.Item wrapperCol={{ offset: 6, span: 16 }}>
              <div className="flex gap-4">
                <Button type="primary" htmlType="submit">
                  Edit
                </Button>
              </div>
              </Form.Item>
            </Form>
        : <div className="flex w-full justify-center items-center p-8">
          <Spin />
        </div>
        }
      </div>
    </main>
  )
}
