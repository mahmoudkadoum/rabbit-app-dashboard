"use client"
import Link from "next/link";
import { DeleteOutlined } from '@ant-design/icons';
import { EyeInvisibleOutlined, EyeTwoTone } from '@ant-design/icons';
import axios from 'axios';
import { useEffect, useState } from "react";
import { baseUrl } from "@/env";
import {
  Button,
  Form,
  Input,
  InputNumber,
  Select,
  Spin,
  message,
  Popconfirm
} from 'antd';

import { Space, Table, Tag, Modal } from 'antd';
const { Column } = Table;

const formItemLayout = {
  labelCol: {
    xs: { span: 24 },
    sm: { span: 6 },
  },
  wrapperCol: {
    xs: { span: 24 },
    sm: { span: 14 },
  },
};

export default function Home() {
  const [form] = Form.useForm();
  const [isLoading, setisLoading] = useState(false);
  const [servicesData, setservicesData] = useState(undefined);
  const [perPage, setperPage] = useState(0);
  const [total, settotal] = useState(0);
  const [currentPage, setcurrentPage] = useState(1);

  const fetchData = async (page =1) => {
    console.log(currentPage)
    const token = localStorage.getItem('token');
    setisLoading(true);
  
    try {
      const response = await axios.get(`${baseUrl}/services`, {
        params: {
          page,
          "order[id]":'asc'
        },
        headers: {
          'Authorization': `Bearer ${token}`
        }
      });
  
      if (response.data.status === 200) {
        console.log(response.data.data);
        setperPage(response.data.meta.per_page);
        setcurrentPage(response.data.meta.current_page);
        settotal(response.data.meta.total);
        setservicesData(response.data.data);
      }
        } catch (error) {
      if (error.response.status === 401) {
        router.push('/login')
      }
      console.log(error);
    } finally {
      setisLoading(false);
    }
  };

  const handleOnPageChange = (page) => {
    fetchData(page)
  }

  useEffect(() => {
    document.title = 'Services'
    fetchData()
  }, [])

  const [open, setOpen] = useState(false);
  const [confirmLoading, setConfirmLoading] = useState(false);


  const showModal = () => {
    setOpen(true);
  };


  const [file, setFile] = useState(null);


  const onFileChange = (e) => {
    console.log(e.target.files[0]);
    setFile(e.target.files[0]);
  };


  const addService = async () => {
    setConfirmLoading(true);
    try {
      const token = localStorage.getItem('token');
      const formData = new FormData();
      formData.append('services', file);
      const response = await axios.post(`${baseUrl}/services`, formData, {
        headers: {
          'Authorization': `Bearer ${token}`,
          'Content-Type': 'multipart/form-data',
        },
      });
      if (response.data.status === 200) {
        fetchData()
        setOpen(false);
        setFile(null)
        message.success('Success')
      }
        } catch (error) {
      if (error.response.status === 401) {
        router.push('/login')
      }
      message.error(error.message);
    } finally {
      setConfirmLoading(false);
    }
  };

  const handleCancel = () => {
    setOpen(false);
    setFile(null)
  };

  const confirmDelete = async (e, id) => {
    try {
      const token = localStorage.getItem('token');
      const response = await axios.delete(`${baseUrl}/services/${id}`, {
        headers: {
          'Authorization': `Bearer ${token}`,
        },
      });
      if (response.data.status === 200) {
        message.success('Deleted Successfully');
        fetchData()
      }
        } catch (error) {
      if (error.response.status === 401) {
        router.push('/login')
      }
      console.log(error);
      message.success(error);
    } finally {
      setConfirmLoading(false);
    }
  };

  return (
    <main className="flex min-h-[85.75vh] flex-col items-center p-0 overflow-x-hidden bg-white">

      <div className="w-full flex py-4 justify-end">
        <Button type="primary" onClick={showModal} className="bg-main_blue-100">Add Service</Button>
        <Modal title={'Add Service'} open={open} confirmLoading={confirmLoading} footer={null} onCancel={handleCancel}>

          {open && <Form {...formItemLayout} variant="filled" style={{ maxWidth: 600 }} onFinish={addService} form={form}>
            <Form.Item
            hasFeedback
            label="Service File"
            name="services"
            rules={[
              { required: true, message: 'Please upload an Excel file!' },
            ]}
          >
            <Input type="file" accept=".xlsx, .xltx" onChange={e => onFileChange(e)}/>
          </Form.Item>


            <Form.Item wrapperCol={{ offset: 6, span: 16 }}>
            <div className="flex gap-4">
              <Button type="primary" htmlType="submit">
                Submit
              </Button>
              <Button onClick={handleCancel}>
                cancel
              </Button>
            </div>
            </Form.Item>
          </Form>}

        </Modal>
      </div>

      <div className="flex w-full">
        {!isLoading ? <Table className="w-full" dataSource={servicesData} 
        pagination={{
          current: currentPage,
          pageSize: perPage,
          total:total,
          onChange: handleOnPageChange
        }}>
          <Column title="Name" dataIndex="name" key="name" />
          <Column title="Price" dataIndex="price" key="price" />
          <Column title="Currency" dataIndex="currency" key="currency" />
          <Column title="Created At" dataIndex="created_at" key="created_at" />
          <Column title="Updated At" dataIndex="updated_at" key="updated_at" />
          <Column title="Country" key="country" render={(_, record) => (<p>{record.country.name}</p>)}/>
          <Column title="Region" key="region" render={(_, record) => (<p>{record.region.name}</p>)}/>
          <Column
            title="Tags"
            dataIndex="is_active"
            key="is_active"
            render={(is_active) => (
              <>
                <Tag color={parseInt(is_active) === 1 ? 'green' : 'volcano'} key={parseInt(is_active)}>
                  {parseInt(is_active) === 1 ? 'Active' : 'Inactive'}
                </Tag>
              </>
            )}
          />
          <Column
            title="Action"
            key="action"
            render={(_, record) => (
              <Space size="middle" key={record.id}>
                <Popconfirm
                  title="Delete"
                  description="Are you sure to delete?"
                  onConfirm={(e) => confirmDelete(e, record.id)}
                  okText="Yes"
                  cancelText="No"
                >
                  <DeleteOutlined className="transition hover:scale-125 text-main_color-100 cursor-pointer"/>
                </Popconfirm>
              </Space>
            )}
          />
        </Table> 
        : <div className="flex w-full justify-center items-center p-8">
          <Spin />
        </div>
        }
      </div>
    </main>
  )
}
