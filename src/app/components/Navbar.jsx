"use client"
import React, { useEffect, useState } from 'react'
import Link from 'next/link';
import { Fragment } from 'react';
import { usePathname, useRouter } from 'next/navigation';
import { Space, Dropdown, Button } from "antd";
import axios from 'axios';
import { baseUrl } from '@/env';
import { AiFillCaretDown, AiOutlineArrowRight } from 'react-icons/ai';
import { DownOutlined, SmileOutlined } from '@ant-design/icons';
import { IoMenu } from "react-icons/io5";

function Navbar() {


  const router = useRouter()

  const [Regional, setRegional] = useState();
  const [RegionalSectionContent, setRegionalSectionContent] = useState(null);
  const [Local, setLocal] = useState();
  const [LocalSectionContent, setLocalSectionContent] = useState(null);


  useEffect(() => {
    axios.get(`${baseUrl}/home`)
    .then(function (response) {
      // handle success
      console.log(response.data.data);
      if (response.data.status === 200) {
        setLocal(response.data.data.local)
      }
    })
    .catch(function (error) {
      // handle error
      console.log(error);
    })
    .finally(function () {
      // always executed
    });
    axios.get(`${baseUrl}/regional`)
    .then(function (response) {
      // handle success
      console.log(response.data.data);
      if (response.data.status === 200) {
        setRegional(response.data.data)
      }
    })
    .catch(function (error) {
      // handle error
      console.log(error);
    })
    .finally(function () {
      // always executed
    });
  }, [])

  useEffect(() => {
    setRegionalSectionContent(
      <div>
        {Regional && Regional?.map((region) => 
          <div className="flex flex-col gap-y-3">
            <button 
            onClick={() => router.push(`/local?regional=${region.id}`)}
            className="border-none shadow-none border bg-transparent flex justify-start text-[#1E1E1E] text-lg  transition-all hover:scale-105">
              {region.name}
            </button>
          </div>
        )}
      </div>
    )
  }, [Regional])

  useEffect(() => {
    setLocalSectionContent(
      <div className="flex flex-wrap w-full gap-y-2">
      {Local && Local.map ((item) => (
        <div key={item.label} className="w-1/3 px-1">
          <Button 
          onClick={() => router.push(`/plans/view?id=${item.id}`)}
          className="flex items-center justify-start gap-2 w-full border-none transition-all hover:scale-105 shadow-none">
            <img className="w-6" src={item.image} />
            <p className="text-main_dark-100">{item.name}</p>  
          </Button>
        </div>
      ))}
      </div>
    )
  }, [Local])
  
  
  const Pathname = usePathname()
  const [IsLogedIn, setIsLogedIn] = useState(true)

  useEffect(() => {
    setIsLogedIn(!!localStorage.getItem('token'))
  }, [Pathname])
  

  const items = [
    {
      label: (
        <div className='w-fit flex gap-20 p-16'>
          <div className='flex flex-col w-60 gap-12'>
            <p className='text-lg font-semibold'>REGIONAL ESIM PLANS</p>
            <div className='flex flex-col'>
              {RegionalSectionContent}
            </div>
          </div>
          <div className='flex flex-col w-[55rem] gap-12'>
            <p className='text-lg font-semibold'>POPULAR TRAVEL ESIM PLANS</p>
            <div className='flex flex-col'>
            <div>
              {LocalSectionContent}
            </div>
            </div>
          </div>
        </div>
      ),
      key: '0',
    },
  ];


  const hamburgerMenu = [
    {
      key: '1',
      label: (
        <Link legacyBehavior href="/plans/view">
          <a className="text-white hover:text-main_color-100 mr-4">For Business</a>
        </Link>
      ),
    },
    {
      key: '2',
      label: (
        <Link legacyBehavior href="/how-it-work">
          <a className="text-white hover:text-main_color-100 mr-4">How it Works</a>
        </Link>
      ),
    },
    {
      key: '3',
      label: (
        <Link legacyBehavior href="/coverage">
        <a className="text-white hover:text-main_color-100 mr-4">Coverage</a>
      </Link>
      ),
    },
    {
      key: '4',
      label: (
        <Link legacyBehavior href="/purchasing">
        <a className="text-white hover:text-main_color-100 mr-4">Help</a>
      </Link>
      ),
    },
    {
      key: '5',
      label: 
        !IsLogedIn ?
          (<div className='flex gap-1 justify-self-end'>
            <button 
            className="bg-transparent hover:text-main_color-100  py-2 px-4 rounded">
              <Link href="/login" scroll={false}>
                Login
              </Link>
            </button>
            <button className="bg-gradient-to-r from-main_color-100 to-main_color-200 text-white py-2 px-8 rounded-3xl ">
              Sign Up
            </button>
          </div>)
          : 
          (<div className='flex gap-1 justify-self-end'>
            <button 
            className="bg-transparent hover:text-main_color-100  py-2">
              <Link href="/login" scroll={false}>
                Logout
              </Link>
            </button>
          </div>)
      ,
    },
  ];


  return (
    <Fragment>
      {/* Navbar */}
      {!['/login','/signup'].includes(Pathname) && 
      <nav className="fixed bg-gradient-to-b from-gray-900 to-transparent top-0 left-0 right-0 backdrop-blur-sm py-4" style={{zIndex:'1000'}}>

        <div className="flex items-center justify-between px-20 gap-20 xs:hidden sm:hidden md:hidden">
        {/* Logo */}
        <div className="flex items-center justify-start">
          <Link href="/" scroll={false}>
              <img 
              className="w-24 scale-125 drop-shadow-xl" 
              src="/Company Logo.svg" 
              alt="Logo" />
          </Link>
        </div>
        {/* Links */}
        <div className='flex gap-8'>
          {/* <Link legacyBehavior href="/">
            <a className="text-white hover:text-main_color-100 mr-4">Travel eSIM</a>
          </Link> */}

          <Dropdown menu={{ items }} placement='bottom'>
            <a onClick={(e) => e.preventDefault()}>
              <Space className='cursor-pointer'>
              Travel eSIM
                {/* <DownOutlined /> */}
              </Space>
            </a>
          </Dropdown>
          <Link legacyBehavior href="/plans/view">
            <a className="text-white hover:text-main_color-100 mr-4">For Business</a>
          </Link>
          <Link legacyBehavior href="/how-it-work">
            <a className="text-white hover:text-main_color-100 mr-4">How it Works</a>
          </Link>
          <Link legacyBehavior href="/coverage">
            <a className="text-white hover:text-main_color-100 mr-4">Coverage</a>
          </Link>
          <Link legacyBehavior href="/purchasing">
            <a className="text-white hover:text-main_color-100 mr-4">Help</a>
          </Link>
          <div>
          <img className='w-6 h-6' src="/search.svg" />
          </div>
        </div>

          {/* Login button */}
          {!IsLogedIn ?
          <div className='flex gap-1 justify-self-end'>
            <button 
            className="bg-transparent hover:text-main_color-100 text-white py-2 px-4 rounded">
              <Link href="/login" scroll={false}>
                Login
              </Link>
            </button>
            <button className="bg-gradient-to-r from-main_color-100 to-main_color-200 text-white py-2 px-8 rounded-3xl ">
              Sign Up
            </button>
          </div>
          : 
          <div className='flex gap-1 justify-self-end'>
            <button 
            className="bg-transparent hover:text-main_color-100 text-white py-2 px-4 rounded">
              <Link href="/login" scroll={false}>
                Logout
              </Link>
            </button>
          </div>}
        </div>

        <div className='w-full justify-end py-4 px-8 hidden xs:flex sm:flex md:flex'>
        <Dropdown menu={{items:hamburgerMenu,}}>
            <a onClick={(e) => e.preventDefault()}>
              <Space>
                <IoMenu className='w-8 h-8'/>
              </Space>
            </a>
          </Dropdown>
        </div>
      </nav>}
    </Fragment>
  );
}

export default Navbar;
