"use client"
import React, { useState } from "react";
import {Tabs,TabsHeader,TabsBody,Tab,TabPanel,Card,CardBody,Input,Typography,} from "@material-tailwind/react";
import Link from 'next/link';
import { Switch, Button } from 'antd';
import PhoneInput from 'react-phone-input-2'
import 'react-phone-input-2/lib/style.css'
import { useRouter } from "next/navigation";
import { loginUrl } from "@/env";



export default function AuthForm({type}) {

  const router = useRouter();


    const [loading, setLoading] = useState();
    const [first_name, setFirst_name] = useState();
    const [last_name, setLast_name] = useState();
    const [email, setEmail] = useState();
    const [password, setPassword] = useState();
    const [phone, setPhone] = useState();
    const [phone_code, setPhone_code] = useState();
    const [mail_is_subscribed, setMail_is_subscribed] = useState(0);

    const onChange = (checked) => {
        setMail_is_subscribed(checked ? 1 : 0)
    };

    const onPhoneChange = (value, data) => {
        setPhone(value.replace(data.dialCode, ''))
        setPhone_code(data.dialCode)
    }

    const handleLogin = async () => {

        setLoading(true)

        const res = await fetch(
        `${loginUrl}/auth/login`,
        {
            method: "POST",
            headers: {
            "Content-Type": "application/json",
            },
            body: JSON.stringify(
                {
                    email,
                    password
                }
            ),
            cache: "no-store",
        }
        );
        const data = await res.json();
        console.log(data.data)
        if (data.status === 200) {
        localStorage.setItem('token', data.data.token);
        router.push('/');
        setLoading(false)
        console.log(data)
        }
    };

    const handleRegister = async () => {

        setLoading(true)

        const res = await fetch(
            `${baseUrl}/auth/register`,
        {
            method: "POST",
            headers: {
            "Content-Type": "application/json",
            },
            body: JSON.stringify(
                {
                    email,
                    password,
                    first_name,
                    last_name,
                    phone,
                    phone_code,
                    mail_is_subscribed
                }
            ),
            cache: "no-store",
        }
        );
        const data = await res.json();
        console.log(data.data)
        if (data.status === 200) {
        localStorage.setItem('token', data.data.token);
        setLoading(false)
        router.push('/');
        console.log(data)
        }
    };





    function formatCardNumber(value) {
        const val = value.replace(/\s+/g, "").replace(/[^0-9]/gi, "");
        const matches = val.match(/\d{4,16}/g);
        const match = (matches && matches[0]) || "";
        const parts = [];
        
        for (let i = 0, len = match.length; i < len; i += 4) {
            parts.push(match.substring(i, i + 4));
        }
        
        if (parts.length) {
            return parts.join(" ");
        } else {
            return value;
        }
        }
        
        function formatExpires(value) {
        return value
            .replace(/[^0-9]/g, "")
            .replace(/^([2-9])$/g, "0$1")
            .replace(/^(1{1})([3-9]{1})$/g, "0$1/$2")
            .replace(/^0{1,}/g, "0")
            .replace(/^([0-1]{1}[0-9]{1})([0-9]{1,2}).*/g, "$1/$2");
        }
    

    return (
    <main className="loginPage flex-col items-center justify-between p-0 overflow-x-hidde bg-white">
        <section className="flex justify-center items-center w-full h-full">
            <div className="flex flex-col justify-center items-center w-full gap-y-8">
                <Link href="/" scroll={false}>
                    <img 
                    className="w-24 scale-125 drop-shadow-xl" 
                    src="/Company Logo Dark.svg" 
                    alt="Logo" />
                </Link>
                <p className="text-gray-900 text-xl font-medium">Welcome Back!</p>

                <Card className="w-96 p-5">
                    <CardBody>
                        <form className="flex flex-col gap-4">
                            <div>
                                <Typography variant="small" color="blue-gray" className="mb-2 font-medium">Email Or Username</Typography>
                                <Input
                                    value={email}
                                    onChange={(e) => setEmail(e.target.value)}
                                    type="email"
                                    placeholder="Email Or Username"
                                    className=" !border-t-blue-gray-200 focus:!border-t-gray-900"
                                    labelProps={{
                                    className: "before:content-none after:content-none",
                                    }}/>
                            </div>
                            <div>
                                <Typography variant="small" color="blue-gray" className="mb-2 font-medium">Password</Typography>
                                <Input
                                    value={password}
                                    onChange={(e) => setPassword(e.target.value)}
                                    type="password"
                                    placeholder="Password"
                                    className=" !border-t-blue-gray-200 focus:!border-t-gray-900"
                                    labelProps={{
                                    className: "before:content-none after:content-none",
                                    }}/>
                            </div>

                            {/* <Button size="lg" disabled={!email && !password} onClick={handleLogin}>LOGIN</Button> */}
                            <Button size="lg" disabled={!email && !password} onClick={handleLogin} loading={loading} 
                            className="bg-gradient-to-r from-main_color-100 to-main_color-200 text-white font-bold px-8 rounded-3xl shadow-inne hover:text-white hover:border-none"
                            >LOGIN</Button>
                        </form>
                    </CardBody>
                    </Card>
            </div>
        </section>
    </main>
    )
}
