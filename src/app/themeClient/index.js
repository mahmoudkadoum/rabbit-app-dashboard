"use client";

import {
    ThemeProvider,
    Button,
    Tabs,
    TabsHeader,
    TabsBody,
    Tab,
    TabPanel,
    Card,
    CardHeader,
    CardBody,
    Input,
    Typography,
    Select,
    Option,
    Menu,
    MenuHandler,
    MenuList,
    MenuItem,
    Spin
} from "@material-tailwind/react";

export { 
    ThemeProvider, 
    Button,
    Tabs,
    TabsHeader,
    TabsBody,
    Tab,
    TabPanel,
    Card,
    CardHeader,
    CardBody,
    Input,
    Typography,
    Select,
    Option,
    Menu,
    MenuHandler,
    MenuList,
    MenuItem,
    Spin
};