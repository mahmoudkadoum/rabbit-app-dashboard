"use client"
import Link from "next/link";
import { DeleteOutlined } from '@ant-design/icons';
import { EyeInvisibleOutlined, EyeTwoTone } from '@ant-design/icons';
import axios from 'axios';
import { useEffect, useState } from "react";
import { baseUrl } from "@/env";
import { MdEdit } from "react-icons/md";
import {
  Button,
  Form,
  Input,
  InputNumber,
  Select,
  Spin,
  message,
  Popconfirm
} from 'antd';

import { Space, Table, Tag, Modal } from 'antd';
import { useRouter } from "next/navigation";
const { Column } = Table;

const formItemLayout = {
  labelCol: {
    xs: { span: 24 },
    sm: { span: 6 },
  },
  wrapperCol: {
    xs: { span: 24 },
    sm: { span: 14 },
  },
};

export default function Home() {
const router = useRouter()


  const [form] = Form.useForm();
  const [isLoading, setisLoading] = useState(false);
  const [usersData, setusersData] = useState(undefined);
  const [perPage, setperPage] = useState(0);
  const [total, settotal] = useState(0);
  const [currentPage, setcurrentPage] = useState(1);
  const [selectedUserTypeFilter, setselectedUserTypeFilter] = useState(undefined);
  const [selectedUserStatusFilter, setselectedUserStatusFilter] = useState(undefined);

  const fetchData = async (page =1, type ,status,) => {
    console.log(currentPage)
    const token = localStorage.getItem('token');
    setisLoading(true);
  
    try {
      const response = await axios.get(`${baseUrl}/users`, {
        params: {
          page,
          type,
          status,
          "order[id]":'asc'
        },
        headers: {
          'Authorization': `Bearer ${token}`
        }
      });
  
      if (response.data.status === 200) {
        console.log(response.data.data);
        setperPage(response.data.meta.per_page);
        setcurrentPage(response.data.meta.current_page);
        settotal(response.data.meta.total);
        setusersData(response.data.data);
      }
        } catch (error) {
      if (error.response.status === 401) {
        router.push('/login')
      }
      console.log(error);
    } finally {
      setisLoading(false);
      setselectedUserTypeFilter(type)
      setselectedUserStatusFilter(status)
    }
  };

  const handleOnPageChange = (page) => {
    fetchData(page, selectedUserTypeFilter, selectedUserStatusFilter)
  }
  const onUsersTypeChange = (value) => {
    fetchData(1, value, selectedUserStatusFilter)
  };
  const onUsersStatusChange = (value) => {
    fetchData(1, selectedUserTypeFilter, value)
  };


  useEffect(() => {
    document.title = 'Home'
    fetchData()
  }, [])

  const [open, setOpen] = useState(false);
  const [confirmLoading, setConfirmLoading] = useState(false);


  const [selectedUser, setselectedUser] = useState('')
  const [selectedUserType, setselectedUserType] = useState('')


  const showModal = () => {
    setselectedUserType('')
    setisEdit(false)
    setselectedUser(null)
    setOpen(true);
  };


  const onSelectUerType = (value) => {
    console.log(value)
    setselectedUserType(value)
  }

  useEffect(() => {
    console.log(form)
    if (!open) {
      form.resetFields();
    }
  }, [open, form]);


  const editUser = async (values) => {
    setConfirmLoading(true);
    console.log(values)
    try {
      const token = localStorage.getItem('token');
      const response = await axios.put(`${baseUrl}/users/${selectedUser.id}`, values, {
        headers: {
          'Authorization': `Bearer ${token}`,
        },
      });
      if (response.data.status === 200) {
        fetchData()
        setOpen(false);
        setselectedUserType('')
        setisEdit(false)
        setselectedUser(null)
        message.success('Success')
      }
        } catch (error) {
      if (error.response.status === 401) {
        router.push('/login')
      }
      console.log(error);
    } finally {
      setConfirmLoading(false);
    }
  };

  const addUser = async (values) => {
    setConfirmLoading(true);
    console.log(values)
    try {
      const token = localStorage.getItem('token');
      const response = await axios.post(`${baseUrl}/users`, values, {
        headers: {
          'Authorization': `Bearer ${token}`,
        },
      });
      if (response.data.status === 200) {
        fetchData()
        setOpen(false);
        setselectedUserType('')
        setisEdit(false)
        setselectedUser(null)
        message.success('Success')
      }
        } catch (error) {
      if (error.response.status === 401) {
        router.push('/login')
      }
      console.log(error);
    } finally {
      setConfirmLoading(false);
    }
  };

  const handleCancel = () => {
    setOpen(false);
    setselectedUserType('')
    setisEdit(false)
    setselectedUser(null)
  };

  const confirmDelete = async (e, id) => {
    try {
      const token = localStorage.getItem('token');
      const response = await axios.delete(`${baseUrl}/users/${id}`, {
        headers: {
          'Authorization': `Bearer ${token}`,
        },
      });
      if (response.data.status === 200) {
        message.success('Deleted Successfully');
        fetchData()
      }
        } catch (error) {
      if (error.response.status === 401) {
        router.push('/login')
      }
      console.log(error);
      message.success(error);
    } finally {
      setConfirmLoading(false);
    }
  };

  const [isEdit, setisEdit] = useState(false)

  const onUserEdit = (user) => {
    setisEdit(true)
    form.setFieldsValue({
      first_name: user.first_name,
      last_name: user.last_name,
      email: user.email,
      password: user.password,
      phone_code: user.phone_code,
      phone: user.phone,
      user_type: user.user_type,
      role_id: user.role_id,
      id: user.id
    });

    setselectedUser(user)
    setselectedUserType(user.user_type)

    setOpen(true);
  }

  return (
    <main className="flex min-h-[85.75vh] flex-col items-center p-0 overflow-x-hidden bg-white">

      <div className="w-full flex py-4 justify-between">
        <div className="flex gap-2 items-center">
          <Select
          placeholder="User Type" 
          value={selectedUserTypeFilter}
          onChange={value => onUsersTypeChange(value)}
            options={[
              { value: 1, label: 'Admin' },
              { value: 2, label: 'Staff' },
              { value: 3, label: 'user' },
            ]}
            />
          <Select 
            placeholder="User status"
            value={selectedUserStatusFilter}
            onChange={value => onUsersStatusChange(value)}
            options={[
              { value: 1, label: 'Active' },
              { value: 0, label: 'Inactive' },
            ]}
            rules={[{ required: true, message: 'This field is Required!' }]}
            />
          <Button type="primary" onClick={() => fetchData()} className="bg-main_blue-100">Reset Filters</Button>
          
        </div>
        <>
          <Button type="primary" onClick={showModal} className="bg-main_blue-100">Add User</Button>
          <Modal title={'Add User'} open={open} confirmLoading={confirmLoading} footer={null} onCancel={handleCancel}>

            {open && <Form {...formItemLayout} variant="filled" style={{ maxWidth: 600 }} onFinish={!isEdit ? addUser : editUser} form={form}>
              <Form.Item 
              hasFeedback
              label="First Name"
              name="first_name"
              validateDebounce={1000}
              rules={[{ required: true, message: 'This field is Required!' }]}
              >
                <Input />
              </Form.Item>

              <Form.Item 
              hasFeedback
              label="Last Name"
              name="last_name"
              validateDebounce={1000}
              rules={[{ required: true, message: 'This field is Required!' }]}
              >
                <Input />
              </Form.Item>

              <Form.Item 
              hasFeedback
              label="Email"
              name="email"
              validateDebounce={1000}
              rules={[
                { required: true, message: 'This field is Required!' }, 
                { type: 'email', message: 'The input is not a valid email!' }]}
              >
                <Input />
              </Form.Item>

              <Form.Item 
              hasFeedback
              label="Password"
              name="password"
              validateDebounce={1000}
              rules={[
                !isEdit && { required: true, message: 'This field is Required!' }, 
                { min: 8, message: 'Password must be at least 8 characters long!' }]}
              >
                <Input.Password
                  placeholder="Password"
                  iconRender={(visible) => (visible ? <EyeTwoTone /> : <EyeInvisibleOutlined />)}
                />
              </Form.Item>

              <Form.Item
                label="Phone Code"
                name="phone_code"
                validateDebounce={1000}
                rules={[{ required: true, message: 'This field is Required!' }]}
              >
                <InputNumber style={{ width: '100%' }} />
              </Form.Item>

              <Form.Item
                label="Phone"
                name="phone"
                validateDebounce={1000}
                rules={[
                  { required: true, message: 'This field is Required!' },
                  {
                    validator: (_, value) => {
                      if (!value || ((value).toString().length >= 8 && (value).toString().length <= 12)) {
                        return Promise.resolve();
                      }
                      return Promise.reject(new Error('Phone number must be between 8 and 12 characters long'));
                    },
                  }
                ]}
              >
                <InputNumber style={{ width: '100%' }} />
              </Form.Item>


              <Form.Item label="User Type" name="user_type" rules={[{ required: true, message: 'This field is Required!' }]}>
                <Select 
                onChange={onSelectUerType}
                options={[
                  { value: 1, label: 'Admin' },
                  { value: 2, label: 'Staff' },
                  { value: 3, label: 'user' },
                ]}
                />
              </Form.Item>

              {[1,2].includes(selectedUserType) && <Form.Item label="Role" name="role_id" rules={[{ required: true, message: 'This field is Required!' }]}>
                <Select 
                options={[
                  { value: 1, label: 'Role 1' },
                  { value: 2, label: 'Role 2' },
                ]}
                rules={[{ required: true, message: 'This field is Required!' }]}
                />
              </Form.Item>}


              <Form.Item wrapperCol={{ offset: 6, span: 16 }}>
              <div className="flex gap-4">
                <Button type="primary" htmlType="submit">
                  Submit
                </Button>
                <Button onClick={handleCancel}>
                  cancel
                </Button>
              </div>
              </Form.Item>
            </Form>}

          </Modal>
        </>
      </div>

      <div className="flex w-full">
        {!isLoading ? <Table className="w-full" dataSource={usersData} 
        pagination={{
          current: currentPage,
          pageSize: perPage,
          total:total,
          onChange: handleOnPageChange
        }}>
          <Column title="First Name" dataIndex="first_name" key="first_name" />
          <Column title="Last Name" dataIndex="last_name" key="last_name" />
          <Column title="Email" dataIndex="email" key="email" />
          <Column title="Phone Code" dataIndex="phone_code" key="phone_code" />
          <Column title="Phone" dataIndex="phone" key="phone" />
          <Column title="Type" dataIndex="user_type_text" key="user_type_text" />
          <Column
            title="Tags"
            dataIndex="is_active"
            key="is_active"
            render={(is_active) => (
              <>
                <Tag color={parseInt(is_active) === 1 ? 'green' : 'volcano'} key={parseInt(is_active)}>
                  {parseInt(is_active) === 1 ? 'Active' : 'Inactive'}
                </Tag>
              </>
            )}
          />
          <Column
            title="Action"
            key="action"
            render={(_, record) => (
              <Space size="middle" key={record.id}>
                <MdEdit onClick={() => onUserEdit(record)} className="transition hover:scale-125 text-main_blue-100 cursor-pointer"/>

                <Popconfirm
                  title="Delete"
                  description="Are you sure to delete?"
                  onConfirm={(e) => confirmDelete(e, record.id)}
                  okText="Yes"
                  cancelText="No"
                >
                  <DeleteOutlined className="transition hover:scale-125 text-main_color-100 cursor-pointer"/>
                </Popconfirm>
              </Space>
            )}
          />
        </Table> 
        : <div className="flex w-full justify-center items-center p-8">
          <Spin />
        </div>
        }
      </div>












    </main>
  )
}
