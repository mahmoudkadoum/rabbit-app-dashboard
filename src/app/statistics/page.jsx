"use client"
import axios from 'axios';
import { useEffect, useState } from "react";
import { baseUrl } from "@/env";
import {
  Spin,
} from 'antd';

import { Table, Tag } from 'antd';
const { Column } = Table;

export default function Home() {
  const [isLoading, setisLoading] = useState(false);
  const [statisticsData, setstatisticsData] = useState(undefined);
  const [perPage, setperPage] = useState(0);
  const [total, settotal] = useState(0);
  const [currentPage, setcurrentPage] = useState(1);

  const fetchData = async (page =1) => {
    console.log(currentPage)
    const token = localStorage.getItem('token');
    setisLoading(true);
  
    try {
      const response = await axios.get(`${baseUrl}/statistics`, {
        params: {
          page,
          "order[id]":'asc'
        },
        headers: {
          'Authorization': `Bearer ${token}`
        }
      });
  
      if (response.data.status === 200) {
        console.log(response.data.data);
        setperPage(response.data.meta.per_page);
        setcurrentPage(response.data.meta.current_page);
        settotal(response.data.meta.total);
        setstatisticsData(response.data.data);
      }
        } catch (error) {
      if (error.response.status === 401) {
        router.push('/login')
      }
      console.log(error);
    } finally {
      setisLoading(false);
    }
  };

  const handleOnPageChange = (page) => {
    fetchData(page)
  }

  useEffect(() => {
    document.title = 'Statistics'
    fetchData()
  }, [])


  return (
    <main className="flex min-h-[85.75vh] flex-col items-center p-0 overflow-x-hidden bg-white">

      <div className="w-full flex py-4 justify-start">
        <p className='text-main_color-200 text-xl font-semibold'>Total Payments: {0} USD</p>
      </div>

      <div className="flex w-full">
        {!isLoading ? <Table className="w-full" dataSource={statisticsData} 
        pagination={{
          current: currentPage,
          pageSize: perPage,
          total:total,
          onChange: handleOnPageChange
        }}>
          <Column title="Name" dataIndex="name" key="name" />
          <Column title="Price" dataIndex="price" key="price" />
          <Column title="Currency" dataIndex="currency" key="currency" />
          <Column title="Purchased At" dataIndex="created_at" key="purchased_at" />
          <Column title="Country" key="country" render={(_, record) => (<p>{record.country.name}</p>)}/>
          <Column title="Region" key="region" render={(_, record) => (<p>{record.region.name}</p>)}/>
          <Column
            title="Tags"
            dataIndex="is_active"
            key="is_active"
            render={(is_active) => (
              <>
                <Tag color={parseInt(is_active) === 1 ? 'green' : 'volcano'} key={parseInt(is_active)}>
                  {parseInt(is_active) === 1 ? 'Active' : 'Inactive'}
                </Tag>
              </>
            )}
          />
        </Table> 
        : <div className="flex w-full justify-center items-center p-8">
          <Spin />
        </div>
        }
      </div>
    </main>
  )
}
