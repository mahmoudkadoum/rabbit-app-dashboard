const withMT = require("@material-tailwind/react/utils/withMT");
/** @type {import('tailwindcss').Config} */
module.exports = withMT({
content: [
    './pages/**/*.{js,ts,jsx,tsx,mdx}',
    './src/components/**/*.{js,ts,jsx,tsx,mdx}',
    './src/app/**/*.{js,ts,jsx,tsx,mdx}',
    './node_modules/@material-tailwind/react/components/**/*.{js,ts,jsx,tsx}',
    './node_modules/@material-tailwind/react/theme/components/**/*.{js,ts,jsx,tsx}',
],
theme: {
    screens: {
    'xs': {'max': '640px'},
    // => @media (max-width: 640px) mobile{ ... }

    'sm': {'min': '640px', 'max': '767px'},
    // => @media (min-width: 640px) small tablet{ ... }

    'md': {'min': '768px', 'max': '1023px'},
    // => @media (min-width: 768px) tablet { ... }

    'lg': {'min': '1024px', 'max': '1279px'},
    // => @media (min-width: 1024px) laptop { ... }

    'xl': {'min': '1280px', 'max': '1535px'},
    // => @media (min-width: 1280px) desktop { ... }

    '2xl': {'min': '1536px'},
    // => @media (min-width: 1536px) large Screen{ ... }
},

extend: {
    colors: {
        main_color: {
            10: "#FFF0F0",
            30: "#1677ff30",
            50: "##1677ff81",
            80: "#1677ff80",
            100: "#1677ff",
            200: "#0b3570",
        },
        main_dark: {
            10: "#A0A0A0",
            100: "#1E1E1E",
        },
        secondary_green: {
            100: "#7FBA7A",
            200: "#94C665",
        },
        main_gray: {
            50: "#b2b3bd",
            100: "#808191",
        },
        secondary_gray: {
            30: "#e4e4e430",
            50: "#e3e6ec",
            100: "#e4e4e4",
        },
        dark_blue: {
            50: "#11142ddd",
            100: "#11142d",
        },

        main_blue: {
            100: '#1677ff'
        }
    },
},

fontFamily: {
    'Poppins': "Poppins",
    'Inter': 'Inter'
}
},
plugins: [
    
],
})
